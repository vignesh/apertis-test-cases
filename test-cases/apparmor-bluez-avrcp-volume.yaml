metadata:
  name: apparmor-bluez-avrcp-volume
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test AppArmor profile for Bluez AVRCP volume up and down commands."

  resources:
    - "A Bluetooth adapter."
    - "An A2DP Source and AVRCP capable phone (https://www.apertis.org/reference_hardware/extras/)"

  macro_ostree_preconditions:
    - reponame: apparmor-bluez-setup
      branch: apertis/v2023dev3
    - reponame: bluez-phone
      branch: apertis/v2023dev3
  pre-conditions:
    - "Please note that connman disables bluetooth by default on a fresh image.
       If it's already enabled, connmanctl will give an \"In progress\" error that
       may be ignored. To enable the device:"
    - $ connmanctl enable bluetooth

  expected:
    - "When the TC has been run, once the log collection script finishes check
       its output."
    - "On Success, aa_get_complaints.sh will find no complaints:"
    - |
        >[...snip useless output...]
        >>> Checking for apparmor complaints ...
        >>> No complaints found!
    - "If something goes wrong, the output will be similar to:"
    - |
        >[...snip useless output...]
        >>> Complaints found, creating report ...
        aa-dump_20180710-100931/
        aa-dump_20180710-100931/complaint_tokens.log
        aa-dump_20180710-100931/audit.log
        aa-dump_20180710-100931/ps_aux.log
        aa-dump_20180710-100931/uname.log
        aa-dump_20180710-100931/journalctl.log
        aa-dump_20180710-100931/image_version
        aa-dump_20180710-100931/os-release
        >>> Report created as /home/chaiwala/aa-dump_20180710-100931.tar.bz2
    - "In this case file a bug report against AppArmor attaching the tar.bz2 file
       created."

run:
  steps:
    - "First ensure that the bluez-avrcp-volume test case is passing before executing the
       apparmor-bluez-avrcp-volume test one (otherwise false-negative might occur):"
    - "Ensure the audio manager is activated:"
    - $ pactl stat
    - "Run btmon before any connection happens:"
    - "$ sudo btmon  | grep -A4 'AV/C: Control'"
    - "Execute the test suite in another terminal:"
    - $ cd $HOME/bluez-phone* ; ./bluez-avrcp-volume.sh
    - "After verifying that the bluez-avrcp-volume test is passing, open a new terminal
       and run the following. Do not yet answer to the question."
    - $ cd $HOME/apparmor-bluez-setup-*; ./aa_get_complaints.sh
    - "Now, run the bluez-avrcp-volume test case again in the previous terminal."
    - $ cd $HOME/bluez-phone ; ./bluez-avrcp-volume.sh
    - "After the bluez-avrcp-volume test ended, answer y to check for complaints."
